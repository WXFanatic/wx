/*

    Copyright 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022  joshua.tee@gmail.com

    This file is part of wX.

    wX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    wX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with wX.  If not, see <http://www.gnu.org/licenses/>.

 */

package joshuatee.wx.settings

import android.content.Context
import android.graphics.Color
import joshuatee.wx.MyApplication
import joshuatee.wx.objects.PolygonWarning
import joshuatee.wx.objects.PolygonWatch
import joshuatee.wx.objects.PolygonType
import joshuatee.wx.radar.Metar
import joshuatee.wx.radar.UtilitySpotter
import joshuatee.wx.radar.SwoDayOne
import joshuatee.wx.ui.UtilityUI

object RadarPreferences {

    //
    // Radar Preferences
    //
    var showRadarWhenPan = true
    var warnings = false
    var locationDotFollowsGps = false
    var dualpaneshareposn = false
    var spotters = false
    var spottersLabel = false
    var obs = false
    var obsWindbarbs = false
    var swo = false
    var cities = false
    var locDot = false
    var lakes = false
    var county = false
    var countyLabels = false
    var countyHires = false
    var stateHires = false
    var watMcd = false
    var mpd = false
    var sti = false
    var hi = false
    var tvs = false
    var showLegend = false
    var drawToolSize = 0
    var obsExtZoom = 0
    var spotterSize = 0
    var aviationSize = 0
    var textSize = 0.0f
    var textSizeDefault = 1.0f
    var locdotSize = 0
    var hiSize = 0
    var tvsSize = 0
    var warnLineSize = 5.0f
    const val warnLineSizeDefault = 5
    var watchMcdLineSize = 4.0f
    var watchMcdLineSizeDefault = 4
    var iconsLevel2 = false
    var gpsCircleLineSize = 0
    var stiLineSize = 0
    var swoLineSize = 0
    var wbLineSize = 0
    var wpcFrontLineSize = 0
    var wxoglCenterOnLocation = false
    var wpcFronts = false
    var spotterSizeDefault = 4
    var aviationSizeDefault = 7
    var locationDotSizeDefault = 8

    var colorTstorm = 0
    var colorTstormWatch = 0
    var colorTor = 0
    var colorTorWatch = 0
    var colorFfw = 0
    var colorMcd = 0
    var colorMpd = 0
    var colorLocdot = 0
    var colorSpotter = 0
    var colorCity = 0
    var colorSti = 0
    var colorHi = 0
    var colorObs = 0
    var colorObsWindbarbs = 0
    var colorCountyLabels = 0
    var nexradBackgroundColor = 0

    var uiAnimIconFrames = "rid"
    var useJni = false
    var drawToolColor = 0
    var blackBg = false

    var wxoglSize = 0
    var wxoglSizeDefault = 13
    var wxoglRememberLocation = false
    var wxoglRadarAutoRefresh = false
    var locationUpdateInterval = 10

    var wxoglZoom = 0.0f
    var wxoglRid = ""
    var wxoglProd = ""
    var wxoglX = 0.0f
    var wxoglY = 0.0f

    fun initRadarPreferences() {
        showRadarWhenPan = getInitialPreference("SHOW_RADAR_WHEN_PAN", "true")
        wpcFronts = getInitialPreference("RADAR_SHOW_WPC_FRONTS", "false")
        locationUpdateInterval = getInitialPreference("RADAR_LOCATION_UPDATE_INTERVAL", 10)
        warnings = getInitialPreference("COD_WARNINGS_DEFAULT", "false")
        locationDotFollowsGps = getInitialPreference("LOCDOT_FOLLOWS_GPS", "false")
        dualpaneshareposn = getInitialPreference("DUALPANE_SHARE_POSN", "true")
        spotters = getInitialPreference("WXOGL_SPOTTERS", "false")
        spottersLabel = getInitialPreference("WXOGL_SPOTTERS_LABEL", "false")
        obs = getInitialPreference("WXOGL_OBS", "false")
        obsWindbarbs = getInitialPreference("WXOGL_OBS_WINDBARBS", "false")
        swo = getInitialPreference("RADAR_SHOW_SWO", "false")
        cities = getInitialPreference("COD_CITIES_DEFAULT", "")
        locDot = getInitialPreference("COD_LOCDOT_DEFAULT", "true")
        lakes = getInitialPreference("COD_LAKES_DEFAULT", "false")
        county = getInitialPreference("RADAR_SHOW_COUNTY", "true")
        watMcd = getInitialPreference("RADAR_SHOW_WATCH", "false")
        mpd = getInitialPreference("RADAR_SHOW_MPD", "false")
        sti = getInitialPreference("RADAR_SHOW_STI", "false")
        hi = getInitialPreference("RADAR_SHOW_HI", "false")
        tvs = getInitialPreference("RADAR_SHOW_TVS", "false")
        countyLabels = getInitialPreference("RADAR_COUNTY_LABELS", "false")
        countyHires = getInitialPreference("RADAR_COUNTY_HIRES", "false")
        stateHires = getInitialPreference("RADAR_STATE_HIRES", "false")
        iconsLevel2 = getInitialPreference("WXOGL_ICONS_LEVEL2", "false")
        showLegend = getInitialPreference("RADAR_SHOW_LEGEND", "false")
        wxoglCenterOnLocation = getInitialPreference("RADAR_CENTER_ON_LOCATION", "false")
        drawToolSize = getInitialPreference("DRAWTOOL_SIZE", 4)
        if (UtilityUI.isTablet()) {
            spotterSizeDefault = 2
            aviationSizeDefault = 3
            locationDotSizeDefault = 4
        }
        obsExtZoom = getInitialPreference("RADAR_OBS_EXT_ZOOM", 7)
        spotterSize = getInitialPreference("RADAR_SPOTTER_SIZE", spotterSizeDefault)
        aviationSize = getInitialPreference("RADAR_AVIATION_SIZE", aviationSizeDefault)
        textSize = getInitialPreference("RADAR_TEXT_SIZE", textSizeDefault)
        locdotSize = getInitialPreference("RADAR_LOCDOT_SIZE", locationDotSizeDefault)
        hiSize = getInitialPreference("RADAR_HI_SIZE", 8)
        tvsSize = getInitialPreference("RADAR_TVS_SIZE", 8)
        warnLineSize = getInitialPreference("RADAR_WARN_LINESIZE", warnLineSizeDefault).toFloat()
        watchMcdLineSize = getInitialPreference("RADAR_WATMCD_LINESIZE", watchMcdLineSizeDefault).toFloat()
        gpsCircleLineSize = getInitialPreference("RADAR_GPSCIRCLE_LINESIZE", 5)
        stiLineSize = getInitialPreference("RADAR_STI_LINESIZE", 3)
        swoLineSize = getInitialPreference("RADAR_SWO_LINESIZE", 3)
        wbLineSize = getInitialPreference("RADAR_WB_LINESIZE", 3)
        wpcFrontLineSize = getInitialPreference("RADAR_WPC_FRONT_LINESIZE", 4)

        uiAnimIconFrames = getInitialPreferenceString("UI_ANIM_ICON_FRAMES", "10")
        useJni = getInitialPreference("RADAR_USE_JNI", "false")
        drawToolColor = getInitialPreference("DRAW_TOOL_COLOR", Color.rgb(255, 0, 0))
        blackBg = getInitialPreference("NWS_RADAR_BG_BLACK", "")

        if (UtilityUI.isTablet()) {
            wxoglSizeDefault = 8
        }
        wxoglSize = getInitialPreference("WXOGL_SIZE", wxoglSizeDefault)
        wxoglRememberLocation = getInitialPreference("WXOGL_REMEMBER_LOCATION", "false")
        wxoglRadarAutoRefresh = getInitialPreference("RADAR_AUTOREFRESH", "false")
        wxoglZoom = MyApplication.preferences.getFloat("WXOGL_ZOOM", wxoglSize.toFloat() / 10.0f)
        wxoglRid = getInitialPreferenceString("WXOGL_RID", "")
        wxoglProd = getInitialPreferenceString("WXOGL_PROD", "N0Q")
        wxoglX = getInitialPreference("WXOGL_X", 0.0f)
        wxoglY = getInitialPreference("WXOGL_Y", 0.0f)

        resetTimerOnRadarPolygons()
    }

    fun radarGeometrySetColors() {
        colorTstorm = getInitialPreference("RADAR_COLOR_TSTORM", Color.rgb(255, 255, 0))
        colorTstormWatch = getInitialPreference("RADAR_COLOR_TSTORM_WATCH", Color.rgb(255, 187, 0))
        colorTor = getInitialPreference("RADAR_COLOR_TOR", Color.rgb(243, 85, 243))
        colorTorWatch = getInitialPreference("RADAR_COLOR_TOR_WATCH", Color.rgb(255, 0, 0))
        colorFfw = getInitialPreference("RADAR_COLOR_FFW", Color.rgb(0, 255, 0))
        colorMcd = getInitialPreference("RADAR_COLOR_MCD", Color.rgb(153, 51, 255))
        colorMpd = getInitialPreference("RADAR_COLOR_MPD", Color.rgb(0, 255, 0))
        colorLocdot = getInitialPreference("RADAR_COLOR_LOCDOT", Color.rgb(255, 255, 255))
        colorSpotter = getInitialPreference("RADAR_COLOR_SPOTTER", Color.rgb(255, 0, 245))
        colorCity = getInitialPreference("RADAR_COLOR_CITY", Color.rgb(255, 255, 255))
        colorSti = getInitialPreference("RADAR_COLOR_STI", Color.rgb(255, 255, 255))
        colorHi = getInitialPreference("RADAR_COLOR_HI", Color.rgb(0, 255, 0))
        colorObs = getInitialPreference("RADAR_COLOR_OBS", Color.rgb(255, 255, 255))
        colorObsWindbarbs = getInitialPreference("RADAR_COLOR_OBS_WINDBARBS", Color.rgb(255, 255, 255))
        colorCountyLabels = getInitialPreference("RADAR_COLOR_COUNTY_LABELS", Color.rgb(234, 214, 123))
        nexradBackgroundColor = getInitialPreference("NEXRAD_RADAR_BACKGROUND_COLOR", Color.rgb(0, 0, 0))
    }

    fun initGenericRadarWarnings(context: Context) {
        PolygonWarning.load(context)
    }

    private fun resetTimerOnRadarPolygons() {
        PolygonWatch.byType[PolygonType.MCD]?.timer?.resetTimer()
        PolygonWatch.byType[PolygonType.MPD]?.timer?.resetTimer()
        PolygonWatch.byType[PolygonType.WATCH]?.timer?.resetTimer()
        PolygonWarning.byType.values.forEach {
            it.timer.resetTimer()
        }
        Metar.timer.resetTimer()
        UtilitySpotter.timer.resetTimer()
        SwoDayOne.timer.resetTimer()
    }

    private fun getInitialPreference(pref: String, initValue: Int): Int =
            MyApplication.preferences.getInt(pref, initValue)

    private fun getInitialPreference(pref: String, initValue: Float): Float =
            MyApplication.preferences.getFloat(pref, initValue)

    private fun getInitialPreference(pref: String, initValue: String): Boolean =
            (MyApplication.preferences.getString(pref, initValue) ?: initValue).startsWith("t")

    private fun getInitialPreferenceString(pref: String, initValue: String): String =
            MyApplication.preferences.getString(pref, initValue) ?: initValue
}
