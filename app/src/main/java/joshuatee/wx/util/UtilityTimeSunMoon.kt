/*

    Copyright 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022  joshua.tee@gmail.com

    This file is part of wX.

    wX is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    wX is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with wX.  If not, see <http://www.gnu.org/licenses/>.

*/

package joshuatee.wx.util

import android.content.Context
import android.text.format.DateFormat
import joshuatee.wx.common.GlobalVariables
import joshuatee.wx.externalSolarized.Solarized
import joshuatee.wx.objects.LatLon
import joshuatee.wx.objects.ObjectDateTime
import joshuatee.wx.radar.RID
import java.time.format.DateTimeFormatter

object UtilityTimeSunMoon {

    // used in ObjectMetar
    fun getSunriseSunsetFromObs(obs: RID): List<ObjectDateTime> {
        val solarized = Solarized(obs.location.lat, obs.location.lon, ObjectDateTime().now)
        val sunrise = ObjectDateTime(solarized.sunrise?.date ?: ObjectDateTime().now)
        val sunset = ObjectDateTime(solarized.sunset?.date ?: ObjectDateTime().now)
        return listOf(sunrise, sunset)
    }

    fun getSunriseSunset(context: Context, latLon: LatLon, shortFormat: Boolean = false): String {
        val am: String
        val pm: String
        val timeFormat = if (!DateFormat.is24HourFormat(context)) {
            "h:mm"
        } else {
            "H:mm"
        }
        if (!DateFormat.is24HourFormat(context)) {
            am = "AM"
            pm = "PM"
        } else {
            am = ""
            pm = ""
        }
        val solarized = Solarized(latLon.lat, latLon.lon, ObjectDateTime().get())
        val formatter = DateTimeFormatter.ofPattern(timeFormat)
        val roundingAdd = 30L
        val sunRiseTime = solarized.sunrise?.date?.plusSeconds(roundingAdd)?.format(formatter)
        val sunSetTime = solarized.sunset?.date?.plusSeconds(roundingAdd)?.format(formatter)
        val dawnTime = solarized.firstLight?.date?.plusSeconds(roundingAdd)?.format(formatter)
        val duskTime = solarized.lastLight?.date?.plusSeconds(roundingAdd)?.format(formatter)
        return if (shortFormat) {
            "$sunRiseTime$am / $sunSetTime$pm"
        } else {
            "Sunrise: $sunRiseTime $am   Sunset: $sunSetTime $pm" + GlobalVariables.newline +
                    "Dawn: $dawnTime $am   Dusk: $duskTime $pm"
        }
    }

    // used by SunRiseCard
    fun getForHomeScreen(context: Context, latLon: LatLon): String =
            getSunriseSunset(context, latLon) + GlobalVariables.newline + ObjectDateTime.gmtTime()
}
